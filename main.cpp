//
//  main.cpp
//  rsh
//
//  Created by 劉彥麟 on 2015/10/10.
//  Copyright © 2015年 劉彥麟. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <ctime>
#include <fstream>
#include <cstring>
#include <string>
#include <fcntl.h>
#include <vector>

// wait()
#include <sys/wait.h>
#include <sys/types.h>

// ERRNO
#include <errno.h>

// signal handling
#include <signal.h>

// verify command exist
#include <sys/stat.h>

// Macro definition
#define SIMPLE 0
#define DEFAULT 1

using namespace std;

// DS definition
typedef struct{
	int count;
	int fd[2];
} Node;

class NumPipe{
	private:
		vector<Node> Vec;
	public:
		void decreaseNum();
		void push(Node);
		int* getFd(int);
		int isZero();
};

// Function prototype
void printMotd();
void printPrompt(int type);

void parseInput(char*, char**);
int cutCmdPipe(char** cmdToken, int pipeCount, char*** &cmd);

void checkExit(const char*);
int checkEnv(char* cmdToken[]);
int checkCmdExist(char* name);
int checkChat(char* cmdToken[]);

void sigintHandler(int param);
void sigchldHandler(int param);
void sigusr1Handler(int param);

void execProg(char* cmdToken[]);
void setEnviron(const char* name, const char* value);

void pipeHandler(char* cmdToken[]);
void cmdHandler(char* cmdToken[]);

int getOutNumber(char* cmdToken[]);
int getErrNumber(char* cmdToken[]);

void sendServer(const char* msg);
void recvServer(char* msg);

// Global variable
const pid_t parentPID = getpid();
int pipefd[1000][2];
NumPipe pool;

//--------------------------------------------------------------------------------------------

// Main function
int main(int argc, const char * argv[]) {
		int chdirRet = chdir("../../ras");

	// Signal handle
	void (*sigintHandle)(int);
	void (*sigchldHandle)(int);
	void (*sigusr1Handle)(int);
	sigintHandle = signal(SIGINT, sigintHandler);
	//sigchldHandle = signal(SIGCHLD, sigchldHandler);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGUSR1, sigusr1Handler);
	
	// Shell motd
    printMotd();
	
	// Set inital PATH
	setEnviron("PATH", "bin:.");
	
	// tell server shell is ready
	sendServer("a");
	
	// Shell main loop
    while(1)
    {
        // prompt for user input, SIMPLE for "%", DEFAULT for "user@host [cwd] %"
        printPrompt(SIMPLE);

		// get user input line
		char cmdInput[15000] = {'\0'};
		cin.getline(cmdInput, 15000);

		// parse input
		char* cmdToken[1000] = {NULL};
		if(feof(stdin))
		{
			// EOF detected, terminate
			cout << endl;
			exit(EXIT_SUCCESS);
		}
		else if(cmdInput[0] == 0)
		{
			// nothing input
			continue;
		}
		else
	        parseInput(cmdInput, cmdToken);
		
		// check if exiting
		checkExit(cmdToken[0]);
		
		// check if setenv / printenv
		if( checkEnv(cmdToken) != 0 )
			continue;
	
		else if( checkChat(cmdToken) != 0 )
		{
			continue;
		}
		
		// start program
		cmdHandler(cmdToken);
	}
    
    return 0;
}

//------------------------------------------------------------------------------

void NumPipe::decreaseNum()
{
	//cout << "dec" << endl;
	// decrease all count by 1
	if(Vec.size() == 0)
		return;
	else
	{
		if(Vec[0].count == 0)
		{
			Vec.erase(Vec.begin());
		}
		for(int i = 0 ; i < Vec.size() ; i++)
		{
			Vec[i].count--;
		}
		if(Vec[0].count == 0)
		{
			close(Vec[0].fd[1]);
			//cerr << "parent close 0's 1" << endl;
		}
		return;
	}
}

void NumPipe::push(Node newNode)
{
	//cout << "push" << endl;
	// push newNode in if don't exist
	for(vector<Node>::iterator it = Vec.begin() ; it!= Vec.end() ; it++)
	{
		if(it->count > newNode.count)
		{
			Vec.insert(it, newNode);
			return;
		}
	}
	Vec.push_back(newNode);
	return;
}

int* NumPipe::getFd(int num)
{
	//cout << "getFd" << endl;
	// get fd for specified count
	for(int i = 0 ; i < Vec.size() ; i++)
	{
		if(Vec[i].count == num)
			return Vec[i].fd;
	}
	return NULL;
}

int NumPipe::isZero()
{
	//cout << "isZero" << endl;
	// whether there is something to output
	if(Vec.size() == 0)
		return 0;
	else if(Vec[0].count == 0)
		return 1;
	else
		return 0;
}

//--------------------------------------------------------------------------------------------

void sigintHandler(int param)
{
	// terminate if not parent
	if(getpid() != parentPID)
		exit(EXIT_FAILURE);
}

void sigchldHandler(int param)
{
	// wait for all child process to prevent zombie
	pid_t pid;
	int status;

	while( (pid = waitpid(-1, &status, WNOHANG)) > 0 );
	return;
}

void sigusr1Handler(int param)
{
	char msg[10000] = {'\0'};
	recvServer(msg);
	write(STDOUT_FILENO, msg, strlen(msg));
}

//-------------------------------------------------------------------------------------------

void printMotd()
{
	// Shell motd
	cout << "****************************************" << endl;
	cout << "** Welcome to the information server. **" << endl;
	cout << "****************************************" << endl << endl;
}

void printPrompt(int type)
{
    // Get prompt information
    char hostname[100];
	gethostname(hostname, 100);
    char cwd[1000];
    getcwd(cwd, 1000);
    char homedir[1000];
    strcpy(homedir, getenv("HOME"));
    char* location = strstr(cwd, homedir);
	if(location != NULL)
    {
        location[strlen(homedir)-1] = '~';
        strcpy(location, location+strlen(homedir)-1);
    }

	// Decide which type of prompt to print
	switch(type)
	{
		case SIMPLE:
			cout << "% " << flush;
			break;
		case DEFAULT:
			cout << getlogin() << "@" << hostname << "[" << cwd << "]" << "\n% ";
			break;
	}
}

void parseInput(char* cmd, char** params)
{
	// tokenize input commands
	for(int i = 0 ; i < 15000 ; i++)
	{
		if(cmd[i] == '\r')
			cmd[i] = '\0';
		if(cmd[i] == '\0')
			break;
	}
	
	for(int i = 0 ; i < 15000 ; i++)
	{
		if(cmd[i] == '\0')
			break;
	}
	
	char tmp[100] = {'\0'};
    for(int i = 0 ; i < 15000 ; i++)
    {
        if(i == 0)
            params[i] = strtok(cmd, " ");
        else
            params[i] = strtok(NULL, " ");
        if(params[i] == NULL)
            break;
    }
}

void checkExit(const char* input)
{
	// kill all childs before exiting
	if( (strcmp(input, "exit") == 0) || (strcmp(input, "logout") == 0) )
	{
		signal(SIGQUIT, SIG_IGN);
		kill(-parentPID, SIGQUIT);
		exit(EXIT_SUCCESS);
	}
	else
		return;
}

void cmdHandler(char* cmdToken[])
{
	// check if pipe exists, if not then execute program only
	for(int i = 0 ; i < 1000 ; i++)
	{
		if(cmdToken[i] == NULL)
			break;
		else if( strcmp(cmdToken[i], "|") == 0 || strcmp(cmdToken[i], ">") == 0 )
		{
			pipeHandler(cmdToken);
			return;
		}
	}
	
	execProg(cmdToken);

	return;
}

void execProg(char* cmdToken[])
{
	if(checkCmdExist(cmdToken[0]) < 0)
	{
		cout << "Unknown command: [" << cmdToken[0] << "]." << endl;
		return;
	}
	pool.decreaseNum();

	int num = getOutNumber(cmdToken);
	int numErr = getErrNumber(cmdToken);
	//cout << "num=" << num << ", numErr=" << numErr << endl;
	if(num != 0 && pool.getFd(num) == NULL)
	{
		Node newNode;
		newNode.count = num;
		pipe(newNode.fd);
		pool.push(newNode);
	}
	if(numErr != 0 && pool.getFd(numErr) == NULL)
	{
		Node newNode;
		newNode.count = numErr;
		pipe(newNode.fd);
		pool.push(newNode);
	}
	
	pid_t pid;
    // start programs
    if( (pid = fork()) < 0)
    {
        // fork() failed
        perror("Fork failed, exiting");
        exit(EXIT_FAILURE);
    }
    else if(pid == 0)
	{
		if(num != 0)
		{
			int *fd = pool.getFd(num);
			dup2(fd[1], STDOUT_FILENO);
			if(numErr == 0)
			{
				// Close pipe only if numErr does not exist
				close(fd[1]);
				close(fd[0]);
			}
			//cerr << "child close " << num << "'s 0 and 1" << endl;
		}
		if(numErr != 0)
		{
			int *fd = pool.getFd(numErr);
			dup2(fd[1], STDERR_FILENO);
			close(fd[1]);
			close(fd[0]);
		}
		
		if(pool.isZero())
		{
			int* fd = pool.getFd(0);
			dup2(fd[0], STDIN_FILENO);
			close(fd[0]);
			//cerr << "child close 0's 0" << endl;
		}
		
		// child process
        execvp(cmdToken[0], cmdToken);
		
		exit(EXIT_FAILURE);
	}
    else
    {
		if(pool.isZero())
		{
			close(pool.getFd(0)[0]);
			close(pool.getFd(0)[1]);
			//cerr << "parent close 0's 0" << endl;
		}
		// parent process
		int status;
        wait(&status);
		return;
    }

}

void setEnviron(const char* name, const char* value)
{
	// set environment variable
	setenv(name, value, 1);
}

void pipeHandler(char* cmdToken[])
{
	int pipeCount = 0;
	for(int i = 0 ; i < 1000 ; i++)
	{
		if(cmdToken[i] == NULL)
			break;
		if( strcmp(cmdToken[i], "|") == 0 || strcmp(cmdToken[i], ">") == 0)
			pipeCount++;
	}

	for(int i = 0 ; i < pipeCount ; i++)
	{
		if(pipe(pipefd[i]) < 0)
		{
			cout << "Fail to create pipe" << endl;
			return;
		}
	}

	char*** cmd = NULL;
	
	// Get number pipe first if exist
	int num = getOutNumber(cmdToken);
	int numErr = getErrNumber(cmdToken);
	int fileRedir = cutCmdPipe(cmdToken, pipeCount, cmd);

	// check cmd exist
	for(int i = 0 ; i < pipeCount+1 ; i++)
	{
		if(i == pipeCount && fileRedir == 1)
			break;
		else if( (checkCmdExist(cmd[i][0])) < 0 )
		{
			cout << "Unknown command: [" << cmd[i][0] << "]." << endl;
			return;
		}
	}

	pool.decreaseNum();
	// Number pipe check
	if(num != 0)
	{
		if(pool.getFd(num) == NULL)
		{
			Node newNode;
			newNode.count = num;
			pipe(newNode.fd);
			pool.push(newNode);
		}
	}
	if(numErr != 0)
	{
		if(pool.getFd(numErr) == NULL)
		{
			Node newNode;
			newNode.count = numErr;
			pipe(newNode.fd);
			pool.push(newNode);
		}
	}
	
	// Debug print
	/*	
	for(int i = 0 ; i < pipeCount+1 ; i++)
	{
		for(int j = 0 ; j < 1000 ; j++)
		{
			if(cmd[i][j] == NULL)
				break;
			cout << cmd[i][j] << " ";
		}
		cout << endl;
	}*/

	for(int i = 0 ; i < pipeCount + 1 ; i++)
	{
		if(i == 0)					// First cmd
		{
			pid_t pid;
			if( (pid = fork()) < 0 )
			{
				cerr << "Fork failed, exiting" << endl;
				exit(EXIT_FAILURE);
			}
			else if(pid == 0)
			{
				// Number pipe
				if(pool.isZero())
				{
					int *fd = pool.getFd(0);
					dup2(fd[0], STDIN_FILENO);
					close(fd[0]);
				}
				
				if(pipeCount == 1 && fileRedir == 1)
				{
					int outputFile = creat(cmd[i+1][0], S_IRUSR | S_IWUSR);
					if(outputFile < 0)
					{
						cerr << "open failed" << endl;
						exit(EXIT_FAILURE);
					}
						
					// Dup file fd to STDOUT
					dup2(outputFile, STDOUT_FILENO);
					close(outputFile);

					// Close unused pipes
					close(pipefd[i][0]);
					close(pipefd[i][1]);
					execvp(cmd[i][0], cmd[i]);

					cerr << "exec failed" << endl;
					exit(EXIT_FAILURE);
				}

				dup2(pipefd[i][1], STDOUT_FILENO);
				close(pipefd[i][0]);
				close(pipefd[i][1]);

				execvp(cmd[i][0], cmd[i]);
				cerr << "exec failed" << endl;
			}
			else
			{
				if(fileRedir == 1 && pipeCount == 1)
				{
					close(pipefd[i][0]);
				}
				
				if(pool.isZero())
				{
					close(pool.getFd(0)[0]);
					close(pool.getFd(0)[1]);
				}
				
				int status;
				close(pipefd[i][1]);
				waitpid(-1, &status, 0);
			}
		}
		else if(i == pipeCount)		// Last cmd
		{
			if(fileRedir == 1)
			{
				close(pipefd[i-1][0]);
				continue;
			}

			pid_t pid;
			if( (pid = fork()) < 0 )
			{
				cerr << "Fork failed, exiting" << endl;
				exit(EXIT_FAILURE);
			}
			else if(pid == 0)
			{
				if(num != 0)
				{
					int *fd = pool.getFd(num);
					dup2(fd[1], STDOUT_FILENO);
				}
				if(numErr != 0)
				{
					int *fd = pool.getFd(numErr);
					dup2(fd[1], STDERR_FILENO);
				}
				
				dup2(pipefd[i-1][0], STDIN_FILENO);
				close(pipefd[i-1][0]);

				execvp(cmd[i][0], cmd[i]);
				cerr << "exec failed" << endl;
			}
			else
			{
				close(pipefd[i-1][0]);
			}
		}
		else						// Other cmd
		{
			pid_t pid;
			if( (pid = fork()) < 0 )
			{
				cerr << "Fork failed, exiting" << endl;
				exit(EXIT_FAILURE);
			}
			else if(pid == 0)
			{
				if(i == pipeCount - 1 && fileRedir == 1)
				{
					int outputFile = creat(cmd[i+1][0], S_IRUSR | S_IWUSR);
					if(outputFile < 0)
					{
						cerr << "open failed" << endl;
						exit(EXIT_FAILURE);
					}

					dup2(outputFile, STDOUT_FILENO);
					close(outputFile);
					dup2(pipefd[i-1][0], STDIN_FILENO);
					close(pipefd[i-1][0]);
					close(pipefd[i][1]);
					execvp(cmd[i][0], cmd[i]);
				}
				
				dup2(pipefd[i-1][0], STDIN_FILENO);
				dup2(pipefd[i][1], STDOUT_FILENO);
				close(pipefd[i-1][0]);
				close(pipefd[i][1]);

				execvp(cmd[i][0], cmd[i]);
				cerr << "exec failed" << endl;
			}
			else
			{
				close(pipefd[i-1][0]);
				close(pipefd[i][1]);
			}
		}
		if(fileRedir == 0)
		{
			for(int i = 0 ; i < pipeCount+1 ; i++)
			{
				int status;
				wait(&status);
			}
		}
		else
			for(int i = 0 ; i < pipeCount ; i++)
			{
				int status;
				wait(&status);
			}
	}
}

int checkEnv(char* cmdToken[])
{
	// check if setting / printing environment variables
	if(strcmp(cmdToken[0], "setenv") == 0)
	{
		if(cmdToken[1] == NULL || cmdToken[2] == NULL)
			cout << "Illegal usage, should be [setenv NAME VALUE]" << endl;
		else
			setEnviron(cmdToken[1], cmdToken[2]);
		
		return 1;
	}
	else if( strcmp(cmdToken[0], "printenv") == 0 )
	{
		char* ret;
		if(cmdToken[1] == NULL)
			cout << "Illegal usage, should be [printenv NAME]" << endl;
		else if( (ret = getenv(cmdToken[1])) != NULL)
			cout << cmdToken[1] << "=" << ret << endl;
		else
			cout << "Variable does not exist" << endl;

		return 1;
	}
	else
		return 0;
}

int checkChat(char* cmdToken[])
{
	if( strcmp(cmdToken[0], "who") == 0 )
	{
		char msg[10000] = {'\0'};
		sendServer("who");
		recvServer(msg);
		cout << msg;
		return 1;
	}
	else if( strcmp(cmdToken[0], "tell") == 0 )
	{
		char buf[10000] = {'\0'}, msg[10000] = {'\0'};
		for(int i = 0 ; i < 1000 ; i++)
			if(cmdToken[i] != NULL)
			{
				strcat(buf, cmdToken[i]);
				strcat(buf, " ");
			}
				
		//cout << "Sent to server: " << buf << endl;
		sendServer(buf);
		recvServer(msg);
		if(strcmp(msg, "done") != 0)
			cout << msg;
		return 1;
	}
	else if( strcmp(cmdToken[0], "yell") == 0 )
	{
		char buf[10000] = {'\0'}, msg[10000] = {'\0'};
		for(int i = 0 ; i < 1000 ; i++)
			if(cmdToken[i] != NULL)
			{
				strcat(buf, cmdToken[i]);
				strcat(buf, " ");
			}
		sendServer(buf);
		return 1;
	}
	else if( strcmp(cmdToken[0], "name") == 0 )
	{
		char buf[1000] = {'\0'};
		sprintf(buf, "%s %s", cmdToken[0], cmdToken[1]);
		sendServer(buf);
		char msg[10000] = {'\0'};
		//recvServer(msg);
		
		//if( strcmp(msg, "done") != 0 )
		//	cout << "*** User '" << cmdToken[1] << "' already exists. ***" << endl;
		
		return 1;
	}
	else
		return 0;
}

int cutCmdPipe(char** cmdToken, int pipeCount, char*** &cmd)
{
	// further parse commands if pipe exists
	int retVal = 0;
	cmd = new char**[pipeCount+1];
	
	for(int i = 0 ; i < pipeCount+1 ; i++)
	{
		cmd[i] = new char*[1000];
		for(int j = 0 ; j < 1000 ; j++)
			cmd[i][j] = new char[1000];
	}
	
	int i = 0, currPos = 0, startPos = 0, endPos = 0;
	for(int currPos = 0 ; currPos < 1000 ; currPos++)
	{
		if(i == (pipeCount + 1) )
			return retVal;
		if(cmdToken[currPos] == NULL || (strcmp(cmdToken[currPos], "|") == 0) || (strcmp(cmdToken[currPos], ">") == 0))
		{
			if(cmdToken[currPos] != NULL && strcmp(cmdToken[currPos], ">") == 0)
			{
				retVal = 1;
			}
			endPos = currPos;
			for(int j = 0 ; j < endPos - startPos ; j++)
				strcpy(cmd[i][j], cmdToken[startPos+j]);
			
			for(int j = endPos-startPos ; j < 1000 ; j++)
			{
				delete [] cmd[i][j];
				cmd[i][j] = NULL;
			}

			startPos = currPos + 1;
			i++;
		}
	}
}

int checkCmdExist(char* name)
{
	/*struct stat buf;
	char path[1000] = {'\0'};
	strcat(path, "bin/");
	strcat(path, name);
	if(stat(path, &buf) == 0)
		return 0;
	else
		return -1;*/
	
	pid_t pid;
	if((pid = fork()) < 0)
	{
		cerr << "Cannot fork" << endl;
		exit(EXIT_FAILURE);
	}
	else if(pid == 0)
	{
		fclose(stdout);
		fclose(stdin);
		fclose(stderr);
		
		int ret = execlp(name, name, NULL);
		//cout << "ret=" << ret << endl;
		if(ret != 0)
			exit(10);
		else
			exit(EXIT_SUCCESS);
	}
	else
	{
		int status;
		wait(&status);
		//cout << "WIFEXITED=" << WIFEXITED(status) << ", WEXITSTATUS=" << WEXITSTATUS(status) << endl;
		if(WIFEXITED(status) && WEXITSTATUS(status) == 10)
			return -1;
		else
			return 0;
	}
}

int getOutNumber(char* cmdToken[])
{
	for(int i = 0 ; i < 1000 ; i++)
	{
		if(cmdToken[i] != NULL && cmdToken[i][0] == '|' && cmdToken[i][1] != '\0')
		{
			for(int j = 0 ; j < 10 ; j++)
			{
				if(cmdToken[i][j] == '\0')
				{
					char num[10] = {'\0'};
					strncpy(num, cmdToken[i]+1, j-1);
					cmdToken[i] = NULL;
					for(int k = i ; k < 1000 ; k++)
					{
						if(cmdToken[k+i] != NULL)
						{
							cmdToken[k] = cmdToken[k+1];
						}
						else
							break;
					}
					return atoi(num);
				}
			}
		}
	}

	return 0;
}

int getErrNumber(char* cmdToken[])
{
	for(int i = 0 ; i < 1000 ; i++)
	{
		if(cmdToken[i] != NULL && cmdToken[i][0] == '!' && cmdToken[i][1] != '\0')
		{
			for(int j = 0 ; j < 10 ; j++)
			{
				if(cmdToken[i][j] == '\0')
				{
					char num[10] = {'\0'};
					strncpy(num, cmdToken[i]+1, j-1);
					cmdToken[i] = NULL;
					for(int k = i ; k < 1000 ; k++)
					{
						if(cmdToken[k+1] != NULL)
						{
							cmdToken[k] = cmdToken[k+1];
						}
						else
							break;
					}
					return atoi(num);
				}
			}
		}
	}

	return 0;
}

void sendServer(const char* msg)
{
	write(4, msg, strlen(msg));
}

void recvServer(char* msg)
{
	read(3, msg, 10000);
}
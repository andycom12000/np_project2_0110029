#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>
#include <vector>
#include <string>


#define PORT 5656
#define BUFSIZE 10000

using namespace std;

class Client{
private:
	// private variable
	int readFd[2];
	int writeFd[2];
	int pid;
	int id;
	string name;
	string ip;
	int port;
	
	// private function
	int assignId();
public:
	// constructor and destructor
	Client(int pidInput, char* ipInput, int portInput);
	~Client() {}
	
	// main method
	void changeName(char* nameInput) {name = nameInput;}
	void closeOtherFd();
	void handle(char* buf);
	
	// member access method
	int getId() {return id;}
	int* getReadFd(){return readFd;}
	int* getWriteFd() {return writeFd;}
	int getPid() {return pid;}
	string getName() {return name;}
	string getIp() {return ip;}
	int getPort() {return port;}
	void setPid(int pidInput) {pid = pidInput;}
};

void sigchldHandler(int param);
void parseInput(char*, char**);
void tellAllUser(char*);

vector<Client> clientList;
fd_set rfds, afds;
int  nfds;

int main(int argc, char* argv[])
{
	// Signal Handler
	void (*sigchldHandle)(int);
	sigchldHandle = signal(SIGCHLD, sigchldHandler);
	
	int sockListen = 0, sockConnect = 0;
	struct sockaddr_in server, client;
	int c = sizeof(struct sockaddr_in);
	char msg[2000];
	
	// Socket creation
	if( (sockListen = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		cerr << "Failed to create socket" << endl;
		exit(EXIT_FAILURE);
	}

	// Setup server address info
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);

	// Force bind the socket to PORT
	int on = 1;
	if( (setsockopt(sockListen, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))) < 0 )
	{
		cerr << "Setsockopt failed" << endl;
		exit(EXIT_FAILURE);
	}
	if( (bind(sockListen, (struct sockaddr*)&server, sizeof(server))) < 0 )
	{
		cerr << "Bind failed" << endl;
		exit(EXIT_FAILURE);
	}
	
	// Listen for incoming connection
	listen(sockListen, 10);
	cout << "waiting for connections" << endl;
	
	// Add sockListen to fs_set for monitoring events
	nfds = sockListen + 1;
	FD_ZERO(&afds);
	FD_SET(sockListen, &afds);
	
	while(1)
	{
		//-------------
		int ret;
		memcpy(&rfds, &afds, sizeof(afds));
		if( (ret = select(nfds, &rfds, NULL, NULL, NULL)) < 0 )
		{
			if(errno == EINTR)
				continue;
			cerr << "Failed to select(" << errno << "): " << strerror(errno) << endl;
			exit(EXIT_FAILURE);
		}
		
		// Listen for incoming connections
		if(FD_ISSET(sockListen, &rfds))
		{
			if( (sockConnect = accept(sockListen, (struct sockaddr *)&client, (socklen_t *)&c)) < 0)
			{
				cerr << "Failed to accept" << endl;
				exit(EXIT_FAILURE);
			}
			
			Client *newClient = new Client(0, inet_ntoa(client.sin_addr), ntohs(client.sin_port));

			pid_t pid;
			if( (pid = fork()) < 0 )
			{
				cerr << "fork failed" << endl;
				exit(EXIT_FAILURE);
			}
			else if(pid == 0)
			{
				newClient->closeOtherFd();
				dup2(sockConnect, STDIN_FILENO);
				dup2(sockConnect, STDOUT_FILENO);
				dup2(sockConnect, STDERR_FILENO);
				close(sockListen);
				close(sockConnect);
				
				int newWrite = dup(newClient->getWriteFd()[0]);
				int newRead = dup(newClient->getReadFd()[1]);
				
				close(newClient->getReadFd()[0]);
				close(newClient->getReadFd()[1]);
				close(newClient->getWriteFd()[0]);
				close(newClient->getWriteFd()[1]);
				
				//write(newRead, "a", 1);				
				execl("./shell", "shell", NULL);
				cerr << "execl failed" << endl;
				exit(EXIT_FAILURE);
			}
			else
			{
				newClient->setPid(pid);
				clientList.push_back(move(*newClient));
				close(sockConnect);
				close(newClient->getReadFd()[1]);
				close(newClient->getWriteFd()[0]);
				
				FD_SET(newClient->getReadFd()[0], &afds);
				int max = 4;
				for(int i = 0 ; i < clientList.size() ; i++)
				{
					if(clientList[i].getReadFd()[0] + 1 > max)
						max = clientList[i].getReadFd()[0] + 1;
				}
				nfds = max;
				
				char join[10000] = {'\0'};
				char tmp[10] = {'\0'};
				sprintf(join, "*** User '%s' entered from %s/%d. ***", newClient->getName().c_str(), newClient->getIp().c_str(), newClient->getPort());
				// Make sure the child is ready
				read(newClient->getReadFd()[0], tmp, 10);
				if(strcmp(tmp, "a") == 0)
					tellAllUser(join);
				/*/--------------------
				for(int i = 0 ; i < clientList.size() ; i++)
				{
					cout << "i=" << i << ", pid=" << clientList[i].getPid() << ", read=" << clientList[i].getReadFd()[0] << ", write=" << clientList[i].getWriteFd()[1] << endl;
				}
				*///------------------
				continue;
			}
		}
		
		for(int i = 0 ; i < clientList.size() ; i++)
		{
			if(FD_ISSET(clientList[i].getReadFd()[0], &rfds))
			{
				char buf[1000] = {'\0'};
				//cout << "fd" << clientList[i].getReadFd()[0] << ": childPid=" << clientList[i].getPid() << endl;
				int readRet = read(clientList[i].getReadFd()[0], buf, 1000);
				//cout << "\tReturned " << readRet << " bytes:" << buf << endl;
				clientList[i].handle(buf);
				FD_CLR(clientList[i].getReadFd()[0], &afds);
				FD_SET(clientList[i].getReadFd()[0], &afds);
				sleep(1);
			}
		}
	}

	return 0;
}

//--------------------------------------------------------
// Signal Handler
void sigchldHandler(int param)
{
	//cout << "SIGCHID" << endl;
	pid_t pid;
	int status;
	char name[100] = {'\0'};
	while((pid = waitpid(-1, &status, WNOHANG)) <= 0);
	
	for(vector<Client>::iterator it = clientList.begin() ; it != clientList.end() ; it++)
	{
		if(it->getPid() == pid)
		{
			int *fd1 = it->getReadFd(), *fd2 = it->getWriteFd();
			FD_CLR(fd1[0], &afds);
			close(fd1[0]);
			close(fd2[1]);
			int max = 4;
			for(int i = 0 ; i < clientList.size() ; i++)
			{
				if(clientList[i].getReadFd()[0] + 1 > max)
					max = clientList[i].getReadFd()[0] + 1;
			}
			nfds = max;
			strcpy(name, it->getName().c_str());
			clientList.erase(it);
			//cout << "poped" << endl;
			break;
		}
	}
	
	char buf[10000] = {'\0'};
	sprintf(buf, "*** User '%s' left. ***", name);
	tellAllUser(buf);
}

//-------------------------------------------------------
// Class method definition

Client::Client(int pidInput, char* ipInput, int portInput)
{
	//cout << "Constructor in pid=" << getpid() << endl;
	pid = pidInput;
	pipe(readFd);
	pipe(writeFd);
	name = "(no name)";
	ip = ipInput;
	id = assignId();
	port = portInput;
}

int Client::assignId()
{
	int pres = 1;
	for(int i = 0 ; i < clientList.size() ; i++)
		if(clientList[i].getId() == pres)
		{
			pres++;
			i = 0;
		}
	
	return pres;
}

void Client::closeOtherFd()
{
	for(int i = 0 ; i < clientList.size() ; i++)
	{
		if(pid != clientList[i].getPid())
		{
			close(clientList[i].getReadFd()[0]);
			close(clientList[i].getWriteFd()[1]);
		}
	}
}

void Client::handle(char* buf)
{
	//cout << "handle" << endl;
	if(buf == NULL || strlen(buf) == 0)
		return;
	
	char* cmdToken[1000] = {NULL};
	parseInput(buf, cmdToken);
	char msgSend[10000] = {'\0'};
	
	if( strcmp(cmdToken[0], "who") == 0 )
	{
		int pres = 1, pres_buf = 1;
		
		strcpy(msgSend, "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
		for(int i = 0 ; i < clientList.size() ; i++)
		{
			for(int j = 0 ; j < clientList.size() ; j++)
			{
				if(clientList[j].getId() != pres)
					continue;
				
				pres_buf++;
				char msgTmp[1000];
				sprintf(msgTmp, "%d\t%s\t%s/%d\t", clientList[j].getId(), clientList[j].getName().c_str(), clientList[j].getIp().c_str(), clientList[j].getPort());
				strcat(msgSend, msgTmp);
				
				if(clientList[j].pid == pid)
					strcat(msgSend, "<-me\n");
				else
					strcat(msgSend, "\n");
				
				break;
			}
			if(pres_buf == pres)
			{
				pres_buf = ++pres;
				i--;
			}
			else
				pres = pres_buf;
		}
		//cout << "msgSend:" << endl << msgSend << endl;
		write(writeFd[1], msgSend,strlen(msgSend));
	}
	else if( strcmp(cmdToken[0], "tell") == 0 )
	{
		char msg[1000] = {'\0'};
		int destId = atoi(cmdToken[1]);
		
		for(int i = 0 ; i < clientList.size() ; i++)
			if(clientList[i].getId() == destId)
				strcpy(msg, "done");
		
		if(strcmp(msg, "done") == 0)
		{
			write(writeFd[1], msg, strlen(msg));
			char buf[10000] = {'\0'};
			sprintf(buf, "*** %s told you ***: ", name.c_str());
			for(int i = 1 ; i < 1000 ; i++)
				if(cmdToken[i] != NULL)
				{
					strcat(buf, cmdToken[i]);
					strcat(buf, " ");
				}
					
			for(int i = 0 ; i < clientList.size() ; i++)
				if(clientList[i].getId() == destId)
				{
					write(clientList[i].getWriteFd()[1], buf, strlen(buf));
					kill(clientList[i].getPid(), SIGUSR1);
				}
		}
		else
		{
			sprintf(msg, "*** Error: user #%d does not exist yet. ***\n", destId);
			write(writeFd[1], msg, strlen(msg));
		}
	}
	else if( strcmp(cmdToken[0], "yell") == 0 )
	{
		char buf[10000] = {'\0'};
		sprintf(buf, "*** %s yelled ***: ", name.c_str());
		for(int i = 1 ; i < 1000 ; i++)
		{
			if(cmdToken[i] == NULL)
				break;
			strcat(buf, cmdToken[i]);
			strcat(buf, " ");
		}
		
		tellAllUser(buf);
	}
	else if( strcmp(cmdToken[0], "name") == 0 )
	{
		//cout << "name of id(" << id << ") changed to " << cmdToken[1] << endl;
		char msg[100] = {'\0'};
		strcpy(msg, "done");
		for(int i = 0 ; i < clientList.size() ; i++)
			if(strcmp(cmdToken[1], clientList[i].getName().c_str()) == 0)
				strcpy(msg, "existed");
		
		//write(writeFd[1], msg, strlen(msg));
		if(strcmp(msg, "done") == 0)
		{
			changeName(cmdToken[1]);
			char buf[10000] = {'\0'};
			sprintf(buf, "*** User from %s/%d is named '%s'. ***", getIp().c_str(), getPort(), getName().c_str());
			tellAllUser(buf);
		}
		else
		{
			sprintf(msg, "*** User '%s' already exists. ***", name.c_str());
			write(writeFd[1], msg, strlen(msg));
			kill(pid, SIGUSR1);
		}
	}
	else
		return;
}

//-------------------------------------------------------
void parseInput(char* cmd, char** params)
{
	// tokenize input commands
	for(int i = 0 ; i < 1000 ; i++)
	{
		if(cmd[i] == '\r')
			cmd[i] = '\0';
		if(cmd[i] == '\0')
			break;
	}
	
	for(int i = 0 ; i < 1000 ; i++)
	{
		if(cmd[i] == '\0')
			break;
	}
	
	char tmp[100] = {'\0'};
    for(int i = 0 ; i < 1000 ; i++)
    {
        if(i == 0)
            params[i] = strtok(cmd, " ");
        else
            params[i] = strtok(NULL, " ");
        if(params[i] == NULL)
            break;
    }
}

void tellAllUser(char* buf)
{
	for(int i = 0 ; i < clientList.size() ; i++)
	{
		write(clientList[i].getWriteFd()[1], buf, strlen(buf));
		kill(clientList[i].getPid(), SIGUSR1);
	}
}
CC = g++
FLAGS = -std=c++11

all: shell server

server: server.cpp
	$(CC) $(FLAGS) -o server server.cpp

shell: main.cpp
	$(CC) $(FLAGS) -o shell main.cpp

clean:
	rm shell server
